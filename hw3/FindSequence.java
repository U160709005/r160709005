import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FindSequence {

	public static void main(String[] args) {
		int matrix[][] = readMatrix();
		int [][] coordinate = {{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0}};
		coordinate= findSqn(matrix, 0 , 0, 0, coordinate);
		for (int i = 0 ; i< coordinate.length ; i++){
			matrix[coordinate[i][0]][coordinate[i][1]]= 9-i ; 
		}	
		printMatrix(matrix);

	}

	public static int[][] findSqn(int[][] matrix, int firstPoint, int x , int y , int[][] coordinate){
		if(firstPoint == 0 ) {
			for (int i = 0 ; i<10 ; i++){
				for(int j = 0 ; j< 10 ; j++){
					if(matrix[i][j]==0) {
						coordinate= findSqn(matrix, firstPoint+1 , i ,j , coordinate);
					}
					if (coordinate[9][1]!=0&&coordinate[9][0]!= 0){
						break;
					}
				}
			}
			return coordinate;
		}
		else {
			for (int i = 0 ; i < 10 ; i++){
				for(int j = 0 ; j<10 ; j++){
					if((matrix[i][j]==firstPoint)&&((i==x+1)&&(j==y))||((i==(x-1)&&(j==y))||((i==(x))&&(j==y-1))||((i==x)&&(j==y+1)))){
						coordinate= findSqn(matrix , firstPoint+1, i, j, coordinate);
					}
					if (coordinate[9][1]!=0&&coordinate[9][0]!= 0){
						break;
					}
				}
			}
			coordinate[firstPoint-1][1]=y;
			coordinate[firstPoint-1][0]=x;
			return coordinate ;
		}
	}
 

	private static int[][] readMatrix() {
		int[][] matrix = new int[10][10];
		File file = new File("matrix.txt");

		try {

			Scanner sc = new Scanner(file);
			int i = 0;
			int j = 0;
			while (sc.hasNextLine()) {
				int number = sc.nextInt();
				matrix[i][j] = number;
				if (j == 9)
					i++;
				j = (j + 1) % 10;
				if (i == 10)
					break;
			}
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return matrix;
	}
	
	private static void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
					System.out.print(matrix[i][j]+" ");
			}
			System.out.println();
		}	
	}
}
