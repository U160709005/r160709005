import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

public class FindOccurrences {
	
	public static void main(String[] args) {
		int [][] matrix = readMatrix();
		int [] count = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0,};
	for ( int i = 0 ; i < 10 ; i++){
		for(int j = 0 ; j < 10 ; j++){
		count[matrix[i][j]]++;}
	}
	for (int a =0 ; a < count.length ; a++){
		System.out.println(a+"occurs "+count[a]+" time(s)");
	}
	}
		
		//print the occurrences 
	}


		
	private static int[][] readMatrix(){
		int[][] matrix = new int [10][10];
		File file = new File("matrix.txt");

	    try {

	        Scanner sc = new Scanner(file);
	        int i = 0;
	        int j = 0;
	        while (sc.hasNextLine()) {
	            int number = sc.nextInt();
	            matrix[i][j] = number;
	            if (j == 9)
	            	i++;
	            j = (j + 1) % 10;
	            if (i==10)
	            	break;
	        }
	        sc.close();
	    } 
	    catch (FileNotFoundException e) {
	        e.printStackTrace();
	    }		
		return matrix;
	}
	
}
